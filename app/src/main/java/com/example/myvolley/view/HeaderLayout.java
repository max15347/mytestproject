package com.example.myvolley.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.myvolley.R;

public class HeaderLayout extends View {
    int color;
    Paint mPaint;

    public HeaderLayout(Context context) {
        super(context);
        init();
    }

    public HeaderLayout(Context context,
                        @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public HeaderLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }

    private void init() {
        setWillNotDraw(false);
        mPaint = new Paint();
        mPaint.setColor(Color.parseColor("#00FF00"));
        mPaint.setStrokeWidth(4);
        mPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
        canvas.drawColor(Color.WHITE);
        int height = getLayoutParams().height;
        int width = getMeasuredWidth();
        Log.e("Wtf",height+"   "+width);
        RectF oval = new RectF(-height / 2, -height, width + height / 2, height);
        Log.e("Wtf",oval.bottom+"  "+oval.left+"  "+oval.right+"   "+oval.top);
        canvas.drawArc(oval, 0, 180, false, mPaint);
    }
}