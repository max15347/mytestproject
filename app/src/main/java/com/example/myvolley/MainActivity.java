package com.example.myvolley;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.VolleyError;
import com.example.myvolley.databinding.ActivityMainBinding;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private int prePosition = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(activityMainBinding.getRoot());

        setSupportActionBar(activityMainBinding.toolbar);
        activityMainBinding.appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.e("wtf","ver "+verticalOffset);
                int max = appBarLayout.getTotalScrollRange();
                int gap = max/10;
                Log.e("wtf","max = "+max+" gap =  "+gap);
                if(verticalOffset!=0 && prePosition != verticalOffset) {
                    if (Math.abs(verticalOffset)%gap == 0) {
                        if (prePosition < verticalOffset) {
                            activityMainBinding.bottom.setRotation(activityMainBinding.bottom.getRotation() - 1);
                            Log.e("wtf","rotation = "+activityMainBinding.bottom.getRotation());

                        } else {
                            activityMainBinding.bottom.setRotation(activityMainBinding.bottom.getRotation() + 1);
                            Log.e("wtf","rotation = "+activityMainBinding.bottom.getRotation());
                        }
                    }
                }
                prePosition = verticalOffset;
            }
        });
        activityMainBinding.clickBT.setOnClickListener((v)->{
            new RequestTask(1, null, new VolleyRequest.ResualtListener() {
                @Override
                public void onSuccess(JSONObject jsonObject) {
                    Gson gson = new Gson();
                    Log.e("wtf",gson.toJson(jsonObject));
                }

                @Override
                public void onFail(VolleyError error) {
                    Log.e("wtf",error.toString());
                }
            }).run();
        });
    }
}