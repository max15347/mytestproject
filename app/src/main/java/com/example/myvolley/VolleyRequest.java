package com.example.myvolley;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyRequest {
    private static RequestQueue queue;

    public interface ResualtListener{
        public void onSuccess(JSONObject jsonObject);
        public void onFail(VolleyError error);
    }

    private ResualtListener resualtListener;

    private static RequestQueue getQueue(){
        if(queue == null){
            queue = Volley.newRequestQueue(Appplication.appplication);
        }
        VolleyLog.DEBUG = false;
        return queue;
    }

    private static class RequestTask{
        private ResualtListener resualtListener;
        private RequestTask(ResualtListener resualtListener){
            this.resualtListener = resualtListener;
        }
        private void execute(int method, String targetUrl, @Nullable JSONObject params) {
            JsonObjectRequest request = new JsonObjectRequest(method, targetUrl, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if(resualtListener != null){
                                resualtListener.onSuccess(response);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(resualtListener != null){
                        resualtListener.onFail(error);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> map = new HashMap<>();
                    return map;
                }

                @Override
                public String getBodyContentType() {
                    return "application/x-www-form-urlencoded; charset=UTF-8";
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy
                    .DEFAULT_BACKOFF_MULT) {
            });
            getQueue().add(request);
        }
    }

    public static void getAd(ResualtListener resualtListener){
        new RequestTask(resualtListener).execute(Request.Method.GET,"",new JSONObject());
    }
}
